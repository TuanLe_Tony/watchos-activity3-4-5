//
//  InterfaceController.swift
//  WatchActivities WatchKit Extension
//
//  Created by levantuan on 2019-03-08.
//  Copyright © 2019 levant/Users/levantuan/Desktop/Wearable_IOS/WatchActivities/WatchActivities WatchKit Extensionuan. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity


class InterfaceController: WKInterfaceController, WCSessionDelegate {

    @IBOutlet weak var CountryTable: WKInterfaceTable!
    
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    

    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        //table scroll
        if WCSession.isSupported() {
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }

        
        // 1. create the games
        self.createGameObjects()
        
        self.CountryTable.setNumberOfRows(self.gameList.count, withRowType:"myRow")
        // 2. populate the table
        // 2a. tell ios how many rows are in the table
        // 2b. put the data into the row
        // ---------
        
        // 1. loop through your array
        // 2. take each item in the array and put it in a table row
        for (i, g) in self.gameList.enumerated() {
            let row = self.CountryTable.rowController(at: i) as! CountryObject
            
            row.team1Label.setText(g.teamAName!)
            row.team2Label.setText(g.teamBName!)
            row.locationLabel.setText(g.gameLocation!)
            row.startTimeLabel.setText(g.startTime!)
            row.team1Image.setImage(UIImage(named: g.imageA!))
            row.team2Image.setImage(UIImage(named: g.imageB!))

        }

        
        
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    
    var gameList:[CountryObject] = []
    
    func createGameObjects() {
        let g1 = CountryObject(teamAName: "N.Zealand", teamBName: "Nethelands", gameLocation: "Le Havre", startTime: "09:00 12-June", imageA: "new-zealand",imageB:"netherlands-flag")
        
        let g2 = CountryObject(teamAName: "Chile", teamBName: "Sweden", gameLocation: "Rennes", startTime: "12:00 12-June", imageA: "chile", imageB: "sweden")
        
        let g3 = CountryObject(teamAName: "USA", teamBName: "Thailand", gameLocation: "Reims", startTime: "15:00 12-June",imageA: "America", imageB: "thailand")
        
        let g4 = CountryObject(teamAName: "Nigeria", teamBName: "S.Korea", gameLocation: "Grenoble", startTime: "09:00 13-June", imageA: "nigeria", imageB: "korea")
        
        let g5 = CountryObject(teamAName: "Germany", teamBName: "Spain", gameLocation: "Valenciennes", startTime: "12:00 13-June", imageA: "german-flag", imageB: "spain")
        
        let g6 = CountryObject(teamAName: "France", teamBName: "Norway", gameLocation: "Rice", startTime: "15:00", imageA: "france 13-June", imageB: "norway")
        
        
        gameList.append(g1)
        gameList.append(g2)
        gameList.append(g3)
        gameList.append(g4)
        gameList.append(g5)
        gameList.append(g6)
        
    }


}
