//
//  CountryObject.swift
//  WatchActivities WatchKit Extension
//
//  Created by levantuan on 2019-03-08.
//  Copyright © 2019 levantuan. All rights reserved.
//

import WatchKit

class CountryObject: NSObject {
    
    @IBOutlet weak var SubLabel: WKInterfaceLabel!
    @IBOutlet weak var team2Image: WKInterfaceImage!
    @IBOutlet weak var team1Image: WKInterfaceImage!
    @IBOutlet weak var team2Label: WKInterfaceLabel!
    @IBOutlet weak var team1Label: WKInterfaceLabel!
    @IBOutlet weak var locationLabel: WKInterfaceLabel!
    @IBOutlet weak var startTimeLabel: WKInterfaceLabel!
    // MARK: class properties
    var teamAName:String?
    var teamBName:String?
    var gameLocation:String?
    var startTime:String?
    var imageA:String?
    var imageB:String?
    
    // MARK: contructor
    convenience override init() {
        self.init(teamAName:"", teamBName:"", gameLocation:"", startTime:"",imageA:"",imageB:"" )
    }
    
    init(teamAName:String, teamBName:String, gameLocation:String, startTime:String, imageA:String, imageB:String) {
        
        self.teamAName = teamAName
        self.teamBName = teamBName
        self.gameLocation = gameLocation
        self.startTime = startTime
        self.imageA = imageA
        self.imageB = imageB

        
        
    }

}
