//
//  SubscribeInterfaceController.swift
//  WatchActivities WatchKit Extension
//
//  Created by levantuan on 2019-03-10.
//  Copyright © 2019 levantuan. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

class SubscribeInterfaceController: WKInterfaceController, WCSessionDelegate {

    @IBOutlet weak var CountryTable: WKInterfaceTable!
    
    @IBOutlet weak var nogameLabel: WKInterfaceLabel!
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    var myArray = [String]()
    // WKSession function used by our app
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        // Play a "click" sound when you get the message
        WKInterfaceDevice().play(.click)
        
        // output a debug message to the terminal
        print("Got a message!")
        
        // update the message with a label
        for (game, sub) in message { // I celebrate message and check it
            print("game: '\(game)' : '\(sub)'.")
            //
            if (game == "G1"){
                //put value in to the first row
                
                
                for (_) in self.gameList.enumerated() {
                    let row = self.CountryTable.rowController(at: 0) as! CountryObject
                    if ("Subscribe" == sub as? String){
                        row.SubLabel.setTextColor(UIColor.green)
                        row.SubLabel.setText(sub as? String)
                        
                        myArray.append("Subscribe")
                        self.nogameLabel.setText("Subcribed")
                        
                        print(myArray)
                    }
                    else if ("Unsubscribe" == sub as? String){
                        row.SubLabel.setTextColor(UIColor.red)
                        row.SubLabel.setText(sub as? String)
                        
                        myArray.remove(at: 0)

                        if (myArray.count == 0){
                        self.nogameLabel.setText("No Game")
                        print(myArray)
                        }
                    }
                    
                }
                
            }
            if (game == "G2"){
                for (_) in self.gameList.enumerated() {
                    let row = self.CountryTable.rowController(at: 1) as! CountryObject
                    if ("Subscribe" == sub as? String){
                        row.SubLabel.setTextColor(UIColor.green)
                        row.SubLabel.setText(sub as? String)
                        
                        myArray.append("Subscribe")
        
                        self.nogameLabel.setText("Subcribed")
                        print(myArray)
                    }
                    else if ("Unsubscribe" == sub as? String){
                        row.SubLabel.setTextColor(UIColor.red)
                        row.SubLabel.setText(sub as? String)
                        
                        myArray.remove(at: 0)
                        
                        if (myArray.count == 0){
                        self.nogameLabel.setText("No Game")
                        print(myArray)
                        }
                    }
                    
                }
            }
            if (game == "G3"){
                for (_) in self.gameList.enumerated() {
                    let row = self.CountryTable.rowController(at: 2) as! CountryObject
                    if ("Subscribe" == sub as? String){
                        row.SubLabel.setTextColor(UIColor.green)
                        row.SubLabel.setText(sub as? String)
                        
                        myArray.append("Subscribe")
                        print(myArray)
                        self.nogameLabel.setText("Subcribed")
                    }
                    else if ("Unsubscribe" == sub as? String){
                        row.SubLabel.setTextColor(UIColor.red)
                        row.SubLabel.setText(sub as? String)
                        
                        myArray.remove(at: 0)
                        if (myArray.count == 0){
                            self.nogameLabel.setText("No Game")
                            print(myArray)
                        }
                    }
                    
                }
            }
            if (game == "G4"){
                for (_) in self.gameList.enumerated() {
                    let row = self.CountryTable.rowController(at: 3) as! CountryObject
                    if ("Subscribe" == sub as? String){
                        row.SubLabel.setTextColor(UIColor.green)
                        row.SubLabel.setText(sub as? String)
                        
                        myArray.append("Subscribe")
                        print(myArray)
                        
                        self.nogameLabel.setText("Subcribed")
                    }
                    else if ("Unsubscribe" == sub as? String){
                        row.SubLabel.setTextColor(UIColor.red)
                        row.SubLabel.setText(sub as? String)
                        
                        myArray.remove(at: 0)
                        if (myArray.count == 0){
                            self.nogameLabel.setText("No Game")
                            print(myArray)
                        }
                    }
                    
                }
            }
            if (game == "G5"){
                for (_) in self.gameList.enumerated() {
                    let row = self.CountryTable.rowController(at: 4) as! CountryObject
                    if ("Subscribe" == sub as? String){
                        row.SubLabel.setTextColor(UIColor.green)
                        row.SubLabel.setText(sub as? String)
                        
                        myArray.append("Subscribe")
                        print(myArray)
                        
                        self.nogameLabel.setText("Subcribed")
                    }
                    else if ("Unsubscribe" == sub as? String){
                        row.SubLabel.setTextColor(UIColor.red)
                        row.SubLabel.setText(sub as? String)
                        
                        myArray.remove(at: 0)
                        if (myArray.count == 0){
                            self.nogameLabel.setText("No Game")
                            print(myArray)
                        }
                    }
                    
                }
            }
            if (game == "G6"){
                for (_) in self.gameList.enumerated() {
                    let row = self.CountryTable.rowController(at: 5) as! CountryObject
                    if ("Subscribe" == sub as? String){
                        row.SubLabel.setTextColor(UIColor.green)
                        row.SubLabel.setText(sub as? String)
                        
                        myArray.append("Subscribe")
                        print(myArray)
                        
                        self.nogameLabel.setText("Subcribed")
                    }
                    else if ("Unsubscribe" == sub as? String){
                        row.SubLabel.setTextColor(UIColor.red)
                        row.SubLabel.setText(sub as? String)
                        
                        myArray.remove(at: 0)
                        if (myArray.count == 0){
                            self.nogameLabel.setText("No Game")
                            print(myArray)
                        }
                    }
                    
                }
            }
        }
        
    }
    
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        //table scroll
        if WCSession.isSupported() {
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
        
        
        // 1. create the games
        self.createGameObjects()
        
        self.CountryTable.setNumberOfRows(self.gameList.count, withRowType:"myRow")
        // 2. populate the table
        // 2a. tell ios how many rows are in the table
        // 2b. put the data into the row
        // ---------
        
        // 1. loop through your array
        // 2. take each item in the array and put it in a table row
        for (i, g) in self.gameList.enumerated() {
            let row = self.CountryTable.rowController(at: i) as! CountryObject
            
            row.team1Label.setText(g.teamAName!)
            row.team2Label.setText(g.teamBName!)
            row.locationLabel.setText(g.gameLocation!)
            row.startTimeLabel.setText(g.startTime!)
            row.team1Image.setImage(UIImage(named: g.imageA!))
            row.team2Image.setImage(UIImage(named: g.imageB!))
            
        }
        
        
        
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    
    var gameList:[CountryObject] = []
    
    func createGameObjects() {
        let g1 = CountryObject(teamAName: "N.Zealand", teamBName: "Nethelands", gameLocation: "Le Havre", startTime: "09:00", imageA: "new-zealand",imageB:"netherlands-flag")
        
        let g2 = CountryObject(teamAName: "Chile", teamBName: "Sweden", gameLocation: "Rennes", startTime: "12:00", imageA: "chile", imageB: "sweden")
        
        let g3 = CountryObject(teamAName: "USA", teamBName: "Thailand", gameLocation: "Reims", startTime: "15:00",imageA: "America", imageB: "thailand")
        
        let g4 = CountryObject(teamAName: "Nigeria", teamBName: "S.Korea", gameLocation: "Grenoble", startTime: "09:00", imageA: "nigeria", imageB: "korea")
        
        let g5 = CountryObject(teamAName: "Germany", teamBName: "Spain", gameLocation: "Valenciennes", startTime: "12:00", imageA: "german-flag", imageB: "spain")
        
        let g6 = CountryObject(teamAName: "France", teamBName: "Norway", gameLocation: "Rice", startTime: "15:00", imageA: "france", imageB: "norway")
        
        
        gameList.append(g1)
        gameList.append(g2)
        gameList.append(g3)
        gameList.append(g4)
        gameList.append(g5)
        gameList.append(g6)
        
    }
    
    
    
}
