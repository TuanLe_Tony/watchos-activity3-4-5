//
//  ViewController.swift
//  WatchActivities
//
//  Created by levantuan on 2019-03-08.
//  Copyright © 2019 levantuan. All rights reserved.
//

import UIKit
import WatchConnectivity

class ViewController: UIViewController, WCSessionDelegate {
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
    
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
    
    }
    
    func sessionDidDeactivate(_ session: WCSession) {

    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        print("checking to see if wc session is supported")
        
        if (WCSession.isSupported()) {
            print("Yes it is!")
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
    }
    var G2 = 1 // switching between sub and unsub
    var G1 = 1
    var G3 = 1
    var G4 = 1
    var G5 = 1
    var G6 = 1
    @IBAction func SubOneClicked(_ sender: Any) {
        print("New Zealand vs Nethelands")
        // check if the watch is paired / accessible
        if (WCSession.default.isReachable) {
            // construct the message you want to send
            // the message is in dictionary
            if (G1 == 1){
            let SubOne = ["G1": "Subscribe"]
            // send the message to the watch
            WCSession.default.sendMessage(SubOne, replyHandler: nil)
                G1 = 2
            }
            else if (G1 == 2){
                let SubOne = ["G1": "Unsubscribe"]
                // send the message to the watch
                WCSession.default.sendMessage(SubOne, replyHandler: nil)
                G1 = 1
            }
        }
        
    }
    @IBAction func SubTwoClicked(_ sender: Any) {
        print("Chile vs Sweden")
        // check if the watch is paired / accessible
        if (WCSession.default.isReachable) {
            // construct the message you want to send
            // the message is in dictionary
            if (G2 == 1){
                let SubOne = ["G2": "Subscribe"]
                // send the message to the watch
                WCSession.default.sendMessage(SubOne, replyHandler: nil)
                G2 = 2
            }
            else if (G2 == 2){
                let SubOne = ["G2": "Unsubscribe"]
                // send the message to the watch
                WCSession.default.sendMessage(SubOne, replyHandler: nil)
                G2 = 1
            }
        }
    }
    @IBAction func SubThreeClicked(_ sender: Any) {
        print("USA vs Thailand")
        // check if the watch is paired / accessible
        if (WCSession.default.isReachable) {
            // construct the message you want to send
            // the message is in dictionary
            if (G3 == 1){
                let SubOne = ["G3": "Subscribe"]
                // send the message to the watch
                WCSession.default.sendMessage(SubOne, replyHandler: nil)
                G3 = 2
            }
            else if (G3 == 2){
                let SubOne = ["G3": "Unsubscribe"]
                // send the message to the watch
                WCSession.default.sendMessage(SubOne, replyHandler: nil)
                G3 = 1
            }
        }
    }
    @IBAction func SubFourClicked(_ sender: Any) {
        print("Nigeria vs South Korea")
        // check if the watch is paired / accessible
        if (WCSession.default.isReachable) {
            // construct the message you want to send
            // the message is in dictionary
            if (G4 == 1){
                let SubOne = ["G4": "Subscribe"]
                // send the message to the watch
                WCSession.default.sendMessage(SubOne, replyHandler: nil)
                G4 = 2
            }
            else if (G4 == 2){
                let SubOne = ["G4": "Unsubscribe"]
                // send the message to the watch
                WCSession.default.sendMessage(SubOne, replyHandler: nil)
                G4 = 1
            }
        }
    }
    
    @IBAction func SubFiveClicked(_ sender: Any) {
        print("Germany vs Spain")
        // check if the watch is paired / accessible
        if (WCSession.default.isReachable) {
            // construct the message you want to send
            // the message is in dictionary
            if (G5 == 1){
                let SubOne = ["G5": "Subscribe"]
                // send the message to the watch
                WCSession.default.sendMessage(SubOne, replyHandler: nil)
                G5 = 2
            }
            else if (G5 == 2){
                let SubOne = ["G5": "Unsubscribe"]
                // send the message to the watch
                WCSession.default.sendMessage(SubOne, replyHandler: nil)
                G5 = 1
            }
        }
    }
    
    @IBAction func SubSixClicked(_ sender: Any) {
        print("France vs Norway")
        // check if the watch is paired / accessible
        if (WCSession.default.isReachable) {
            // construct the message you want to send
            // the message is in dictionary
            if (G6 == 1){
                let SubOne = ["G6": "Subscribe"]
                // send the message to the watch
                WCSession.default.sendMessage(SubOne, replyHandler: nil)
                G6 = 2
            }
            else if (G6 == 2){
                let SubOne = ["G6": "Unsubscribe"]
                // send the message to the watch
                WCSession.default.sendMessage(SubOne, replyHandler: nil)
                G6 = 1
            }
        }
    }
}

